const Mongoose = require('mongoose');
const logger = require('../core/logger/app-logger');
const config = require('../core/config/config.dev');


Mongoose.Promise = global.Promise;


const connectToDb = async () => {

  const dbHost = config.dbHost;
  const dbPort = config.dbPort;
  const dbName = config.dbName;
  const dbUsername = config.dbUsername;
  const dbPassword = config.dbPassword;

  const url = config.prodEnv ?  `mongodb+srv://${dbUsername}:${dbPassword}@${dbHost}/${dbName}` : `mongodb://${dbHost}:${dbPort}/${dbName}`;

  try {
      await Mongoose.connect(url, { useNewUrlParser: true });
  } catch (err) {
    logger.error('Could not connect to MongoDB.');
  }
};

module.exports = connectToDb;
