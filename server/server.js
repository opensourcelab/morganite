const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('./core/logger/app-logger');
const morgan = require('morgan');
const winston = require('winston');
const config = require('./core/config/config.dev');
const connectToDb = require('./db/connect');
const APIRouter = require('./routes');

const port = config.serverPort;


logger.stream({
  write(message) {
    logger.info(message);
  },
});
connectToDb();

const app = express();


if (process.env.NODE_ENV === 'production') {
  // Exprees will serve up production assets
  app.use(express.static('client/build'));

  // Express serve up index.html file if it doesn't recognize route
  const path = require('path');
  app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev', { stream: winston.stream.write }));
app.use('/api', APIRouter);

app.get('/', (req, res) => {
  res.send('Invalid endpoint');
});

app.listen(port, () => {
  logger.info('server is running on port ', port);
});
