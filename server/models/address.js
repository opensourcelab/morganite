const mongoose = require('mongoose');


const AddressSchema = new mongoose.Schema({
  active: { type: Boolean, required: true },
  company: { type: String },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  address1: { type: String },
  address2: { type: String },
  post_code: { type: String },
  city: { type: String },
  phone: { type: String },
  date_add: { type: Date },
  date_upd: { type: Date },
  id_order_address: { type: Date },
  id_customer_address: { type: String },

});

const AddressModel = mongoose.model('Address', AddressSchema);

AddressModel.getAllAddresses = () => {
  return AddressModel.find({});
};

AddressModel.getById = (givenId) => {
  return AddressModel.find({ id: givenId });
};

AddressModel.removeById = (givenId) => {
  AddressModel.remove({ id: givenId });
};

AddressModel.addAddress = (addressToAdd) => {
  return addressToAdd.save();
};

module.exports = AddressModel;
