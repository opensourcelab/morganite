const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  userAddressId: String,
}, { collection: 'User' });


const UserModel = mongoose.model('User', UserSchema);

UserModel.getAllUsers = () => {
  return UserModel.find({});
};

UserModel.getByUsername = (userName) => {
  return UserModel.find({ username: userName });
};

UserModel.getByEmail = (Email) => {
  return UserModel.find({ email: Email });
};

UserModel.addUser = (userToAdd) => {
  return userToAdd.save();
};

UserModel.removeByUsername = (userName) => {
  return UserModel.remove({ username: userName });
};

module.exports = UserModel;
