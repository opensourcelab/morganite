const mongoose = require('mongoose');


const ProductSchema = new mongoose.Schema({
  id: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  price: { type: Number, required: true },
  quantity: { type: Number, required: true },
  categoryId: String,
});

const ProductModel = mongoose.model('Product', ProductSchema);

ProductModel.getAllProducts = () => {
  return ProductModel.find({});
};

ProductModel.getById = (givenId) => {
  return ProductModel.find({ id: givenId });
};

ProductModel.removeById = (givenId) => {
  ProductModel.remove({ id: givenId });
};

ProductModel.addProduct = (prodToAdd) => {
  return prodToAdd.save();
};

module.exports = ProductModel;
