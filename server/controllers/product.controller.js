const Product = require('../models/product');
const logger = require('../core/logger/app-logger');

const productController = {};
productController.getAllProducts = async (req, res) => {
  try {
    const products = await Product.getAllProducts();
    logger.info('Getting all products...');
    res.send(products);
  } catch (err) {
    logger.error(`ERR - ProductController: ${err}`);
    res.status(500).send(err);
  }
};

productController.getById = async (req, res) => {
  const productId = req.body.id;
  try {
    const product = await Product.getById(productId);
    logger.info('Getting product by id...');
    res.send(product);
  } catch (err) {
    logger.error(`ERR - ProductController: ${err}`);
    res.send('getById method error');
  }
};

productController.addProduct = async (req, res) => {
  const productToAdd = new Product({
    id: req.body.id,
    name: req.body.name,
    price: req.body.price,
    quantity: req.body.quantity,
    categoryId: req.body.categoryId,
  });
  try {
    const addedProduct = await Product.addProduct(productToAdd);
    logger.info('Creating new product...');
    res.status(201).send(addedProduct);
  } catch (err) {
    logger.error(`ERR - ProductController: ${err}`);
    res.status(500).send(err);
  }
};

productController.removeById = async (req, res) => {
  const productId = req.body.id;
  try {
    const removedProduct = await Product.removeById(productId);
    logger.info('Removing product by id...');
    res.send(`Deleted: ${removedProduct}`);
  } catch (err) {
    logger.error(`ERR - ProductController: ${err}`);
    res.status(500).send(err);
  }
};

module.exports = productController;
