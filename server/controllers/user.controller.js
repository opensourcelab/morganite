const User = require('../models/user');
const logger = require('../core/logger/app-logger');

const userController = {};
userController.getAllUsers = async (req, res) => {
  try {
    const users = await User.getAllUsers();
    logger.info('Getting all users...');
    res.send(users);
  } catch (err) {
    logger.error(`ERR - UserController: ${err}`);
    res.status(500).send(err);
  }
};

userController.getByUsername = async (req, res) => {
  const userName = req.params.id;
  try {
    const user = await User.getByUsername(userName);
    logger.info('Getting user...');
    res.send(user);
  } catch (err) {
    logger.error(`ERR - UserController: ${err}`);
    res.send('getByUsername method error');
  }
};

userController.addUser = async (req, res) => {
  const userToAdd = new User({
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
  });
  try {
    const addedUser = await User.addUser(userToAdd);
    logger.info('Creating new user...');
    res.status(201).send(addedUser);
  } catch (err) {
    logger.error(`ERR - UserController: ${err}`);
    res.status(500).send(err);
  }
};

userController.deleteUser = async (req, res) => {
  const userName = req.body.username;
  try {
    const removedUser = await User.removeByUsername(userName);
    logger.info('Deleted user...');
    res.send(`Deleted: ${removedUser}`);
  } catch (err) {
    logger.error(`ERR - UserController: ${err}`);
    res.status(500).send(err);
  }
};

module.exports = userController;
