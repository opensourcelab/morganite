const Address = require('../models/address');
const logger = require('../core/logger/app-logger');

const addressController = {};
addressController.getAllAddresses = async (req, res) => {
  try {
    const address = await Address.getAllAddresses();
    logger.info('Getting all addresses...');
    res.send(address);
  } catch (err) {
    logger.error(`ERR - AddressController: ${err}`);
    res.status(500).send(err);
  }
};

addressController.getById = async (req, res) => {
  const addressId = req.body.id;
  try {
    const address = await Address.getById(addressId);
    logger.info('Getting address by id...');
    res.send(address);
  } catch (err) {
    logger.error(`ERR - AddressController: ${err}`);
    res.send('getById method error');
  }
};

addressController.addAddress = async (req, res) => {
  const addressToAdd = new Address({
    active: req.body.active,
    company: req.body.company,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    address1: req.body.address1,
    address2: req.body.address2,
    post_code: req.body.post_code,
    city: req.body.city,
    phone: req.body.phone,
    date_add: req.body.date_add,
    date_upd: req.body.date_upd,
    id_order_address: req.body.id_order_address,
    id_customer_address: req.body.id_customer_address,
  });
  try {
    const addedAddress = await Address.addAddress(addressToAdd);
    logger.info('Creating new address...');
    res.status(201).send(addedAddress);
  } catch (err) {
    logger.error(`ERR - AddressController: ${err}`);
    res.status(500).send(err);
  }
};

addressController.removeById = async (req, res) => {
  const addressId = req.body.id;
  try {
    const removedAddress = await Address.removeById(addressId);
    logger.info('Removing address by id...');
    res.send(`Deleted: ${removedAddress}`);
  } catch (err) {
    logger.error(`ERR - AddressController: ${err}`);
    res.status(500).send(err);
  }
};

module.exports = addressController;
