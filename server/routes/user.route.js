const express = require('express');
const userController = require('../controllers/user.controller');

const router = new express.Router();

router.get('/user/:id', (req, res) => {
  userController.getByUsername(req, res);
});
router.get('/user', (req, res) => {
  userController.getAllUsers(req, res);
});

router.post('/user', (req, res) => {
  userController.addUser(req, res);
});


module.exports = router;
