const express = require('express');
const addressController = require('../controllers/address.controller');

const router = new express.Router();

router.get('/address/:id', (req, res) => {
    addressController.getById(req, res);
});

router.get('/address', (req, res) => {
    addressController.getAllAddresses(req, res);
});

router.post('/address', (req, res) => {
    addressController.addAddress(req, res);
});

module.exports = router;