const express = require('express');
const productController = require('../controllers/product.controller');

const router = new express.Router();

router.get('/product', (req, res) => {
  productController.getAllProducts(req, res);
});

router.post('/product', (req, res) => {
  productController.addProduct(req, res);
});

module.exports = router;
