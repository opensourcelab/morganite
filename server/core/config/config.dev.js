const path = require('path');

const config = {};

config.logFileDir = path.join(__dirname, '../../log');
config.logFileName = 'app.log';

//mongo config
config.prodEnv = process.env.prodEnv || false;
config.dbHost = process.env.dbHost || 'localhost';
config.dbPort = process.env.dbPort || '27017';
config.dbName = process.env.dbName || 'morganite';
config.dbUsername = process.env.dbUsername || '';
config.dbPassword = process.env.dbPassword || '';

//node app server port
config.serverPort = process.env.PORT || 3001;

module.exports = config;
