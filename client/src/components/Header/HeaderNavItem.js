import React from 'react';
import {Link} from "react-router-dom";



const HeaderNavItem = (props) => {
  return (
    <div className="header-nav-item r-nav-item">
      <Link className="nav-item-link" to={props.url}>{props.name}</Link>
    </div>
  )
};


export default HeaderNavItem;
