import React from 'react';
import HeaderNavItem from "./HeaderNavItem";
import './Header.scss';
import logo from '../../static/images/logo.png';
import {Link} from "react-router-dom";

function Header() {
  return (
    <div className="fixed-header r-header shadowed">
      <div className="shop-logo r-logo">
        <Link to="/">
          <img src={logo} alt="Logo" className="shop-logo-image"/>
        </Link>
      </div>
      <div className="header-nav">
        <HeaderNavItem name="Home" url="/"/>
        <HeaderNavItem name="Shop" url="/shop"/>
        <HeaderNavItem name="Contact Us" url="/contact-us"/>
      </div>
    </div>
  );
}

export default Header;


