import React, {Component} from 'react';
import carousel1 from '../../static/images/carousel/carousel1.jpg';
import carousel2 from '../../static/images/carousel/carousel2.jpg';
import './HomeCarousel.scss';

export default class HomeCarousel extends Component {
  constructor(props) {
    super(props);


    this.state = {
      currentImage: 0,
      carouselImages: [
        {
          name: 'Sample 1',
          text: {
            header: 'Lorem ipsum',
            content: 'zxcvbnn'
          },
          url: carousel1
        },
        {
          name: 'Sample 2',
          text: {},
          url: carousel2
        }
      ]
    };

    this.leftArrow = React.createRef();
    this.rightArrow = React.createRef();
  }

  filterActive = (index) => {
    let imageHidden = {
      display: 'none'
    };
    let imageShown = {
      display: 'block'
    };
    return this.state.currentImage !== index ? imageHidden : imageShown;
  };
  deactivateArrow = () => {
    let numberOfImages = this.state.carouselImages.length - 1,
      currentImageState = this.state.currentImage,
      rightArrow = this.rightArrow,
      leftArrow = this.leftArrow;

    currentImageState === numberOfImages ? rightArrow.current.classList.add('not-active') : rightArrow.current.classList.remove('not-active');
    currentImageState === 0 ? leftArrow.current.classList.add('not-active') : leftArrow.current.classList.remove('not-active');
  };
  changeImage = (e) => {
    let numberOfImages = this.state.carouselImages.length - 1,
      currTarget = e.currentTarget,
      leftArrow = 'carousel-prev',
      rightArrow = 'carousel-next',
      currentImageState = this.state.currentImage;

    if (currTarget.classList.contains(leftArrow) && (currentImageState > 0)) {
      this.setState({
        currentImage: currentImageState - 1
      });
    } else if (currTarget.classList.contains(rightArrow) && (currentImageState < numberOfImages)) {
      this.setState({
        currentImage: currentImageState + 1
      });
    }

  };

  componentDidUpdate() {
    this.deactivateArrow();
  }
  componentDidMount() {
    this.deactivateArrow();
  }

  render() {
    return (
      <div className='home-carousel r-home-carousel'>
        {this.state.carouselImages.map((image, i) => {
          return (
            <img className={`home-carousel-image carousel-image-${i}`} key={i} src={image.url} alt={image.name}
                 style={this.filterActive(i)}/>
          )
        })
        }
        <span className="home-carousel-button carousel-button-prev carousel-prev"
              onClick={this.changeImage}
              ref={this.leftArrow}>
          <i className="fas fa-chevron-left"/>
        </span>
        <span className="home-carousel-button carousel-button-next carousel-next"
              onClick={this.changeImage}
              ref={this.rightArrow}>
          <i className="fas fa-chevron-right"/>
        </span>

      </div>
    )
  }
}
