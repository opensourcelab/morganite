import React from 'react'
import Header from "../../components/Header/Header";
import HomeCarousel from "../../components/HomeCarousel/HomeCarousel";



const Home = () => {
    return (
        <>
            <Header/>
            <HomeCarousel/>

        </>
    )
};

export default Home;
