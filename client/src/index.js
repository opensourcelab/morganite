import React from 'react';
import ReactDOM from 'react-dom';
import Home from './pages/Home/Home'
import {BrowserRouter as Router, Route} from "react-router-dom";

import './styles/style.scss';

import * as serviceWorker from './serviceWorker';


const Index = () => {
  return (
    <Router>
      <>
        <Route path="/" exact component={Home} />
      </>
    </Router>
  )
};



ReactDOM.render(<Index />, document.getElementById('root'));

serviceWorker.unregister();
