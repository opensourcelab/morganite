##Pre-setup:
1. Install MongoDB: `https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/` (run it and configure)
2. Install Node.js (LTS)  `https://nodejs.org/en/`

##Running project:

0. Clone this repo
1. in project root directory run command `npm install`
2. go to /client and run `npm install` once again
3. go back to root directory and run `npm run dev` to run server and client at once

If you want to run only client go to /client directory and run `npm start`
